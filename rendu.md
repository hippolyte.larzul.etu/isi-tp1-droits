```
# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Nom, Prénom, email: Larzul, Hippolyte, hippolyte.larzul.etu@univ-lille.fr

## Question 1

Le processus ne peut pas écrire, car la permission pour l'utilisateur toto est seulement en lecture.

## Question 2

Pour un répertoire, le caractère 'x' correspond à l'accès pour ouvrir le dossier.
La permission d'entrer dans le dossier 'mydir' est refusée, car le droit d'execution pour le groupe a été retiré.
' -????????? ? ? ? ?           ? data.txt' est retourné, car le dossier est accessible en lecture, mais pas en ouverture. Donc on ne peut pas connaître les droits d'accès au fichier.

## Question 3

```
EUID : 1001
EGID : 1000
RUID : 1001
RGID : 1000
```
Le processus n'arrive pas à ouvrir le fichier mydir/mydata.txt, car l'utilisateur toto ne possède pas les droits d'execution du dossier mydir.


```
EUID : 1000
EGID : 1000
RUID : 1001
RGID : 1000
This is a data text
```
Le processus arrive à ouvrir le fichier mydir/mydata.txt en lecture et afficher le contenu grace au flag set-user-id.

## Question 4

```
EUID : 1001
EGID : 1000
```

## Question 5


La commande 'chfn' permet de modifier les informations personnelles des utilisateurs qui sont stockées dans le fichier '/etc/passwd'.

La commande 'ls -al /usr/bin/chfn' donne le résultat :
'-rwsr-xr-x 1 root root 85064 May 28  2020 /usr/bin/chfn'

L'utilisateur root peut lire, écrire et executer grâce au set-user-id, les autres utilisateurs peuvent lire et executer ce fichier.

Les valeurs du fichier '/etc/passwd' ont bien changées pour l'utilisateur toto :
'toto:x:1001:1000:,10,0102030405,0607080900:/home/toto:/bin/bash'

## Question 6

Les mots de passe des utilisateurs sont stockés dans le fichier '/etc/shadow' pour ne pas être accessibles à tous, et sont hashés car le système coompare des hash, pas des mots de passe.

## Question 7

Les scripts c et bash sont dans le repertoire *question7*.
Il suffit de lancer la commande "./script.sh" pour vérifier les droits. (avec le droit d'execution sur le fichier script.sh)

## Question 8

Le programme et les scripts sont dans le repertoire *question8*.
Il faut lancer la commande "./script.sh".

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 

```
