#!/bin/bash

gcc access_lambda_a.c -o access_lambda_a
gcc access_lambda_b.c -o access_lambda_b
gcc access_admin.c -o access_admin

validate_a=$(./access_lambda_a)
validate_b=$(./access_lambda_b)
validate_admin=$(./access_admin)
if (($validate_a==$validate_b==$validate_admin==1))
then
    echo "Les droits des fichiers sont validés"
else
    echo "Les droits des fichiers ne sont pas validés"
fi
