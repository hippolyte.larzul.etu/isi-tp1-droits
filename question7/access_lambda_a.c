#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

 
int main() {
    int cpt = 0;
    int cpt_no = 0;

    cpt += access("dir_a",F_OK | X_OK | W_OK | R_OK);   // = 0
    
    cpt += access("~/../lambda_c/dir_c",F_OK | R_OK);   // = 0

    cpt_no += access("~/../lambda_c/dir_c",X_OK);   // = -1
    cpt_no += access("~/../lambda_c/dir_c",W_OK);   // = -1

    cpt_no += access("~/../lambda_b/dir_b",F_OK);    // = -1
    cpt_no += access("~/../lambda_b/dir_b",X_OK);    // = -1
    cpt_no += access("~/../lambda_b/dir_b",W_OK);    // = -1
    cpt_no += access("~/../lambda_b/dir_b",R_OK);    // = -1

    if (cpt == 0 && cpt_no == -6){
        printf("1");
        return 1;
    } else {
        printf("0");
        return 0;
    }

    return cpt;
}
