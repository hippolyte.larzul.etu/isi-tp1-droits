#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

 
int main() {
    int cpt = 0;
    int cpt_no = 0;
    
    cpt += access("~/../lambda_c/dir_c",F_OK | R_OK | X_OK | W_OK);   // = 0

    cpt += access("~/../lambda_a/dir_a", F_OK | X_OK | R_OK);
    cpt += access("~/../lambda_b/dir_b", F_OK | X_OK | R_OK);

    cpt_no += access("~/../lambda_b/dir_b",W_OK);    // = -1
    cpt_no += access("~/../lambda_a/dir_a",W_OK);    // = -1


    if (cpt == 0 && cpt_no == -2){
        printf("1");
        return 1;
    } else {
        printf("0");
        return 0;
    }

    return cpt;
}
