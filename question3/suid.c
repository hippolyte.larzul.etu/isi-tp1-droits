#include <stdio.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
    FILE *f;
    char c;

    printf("EUID : %d\n",geteuid());
    printf("EGID : %d\n",getegid());
    printf("RUID : %d\n",getuid());
    printf("RGID : %d\n",getgid());

    f=fopen("mydir/data.txt","rt");
    while((c=fgetc(f))!=EOF){
        printf("%c",c);
    }
    fclose(f);

    return 0;
}
