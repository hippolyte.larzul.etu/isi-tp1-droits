#!/bin/bash

echo "Entrez un fichier à effacer"
read file
echo "Entrez un mot de passe"
read password

group=$(python3 rmg.py $file $password)
gid=$(id -g)
if $group -eq $gid
then
    rm -rf $file
else
    echo "L'utilisateur n'a pas les droits"
fi

