import sys
import os

def main():
    if len(sys.argv) >= 2:
        effacer = sys.argv[1]
    else :
        effacer = input("Fichier à effacer : ")

    g = str(os.getgid())
    password = sys.argv[2]

    f = open("/home/admin/passwd", "a")
    f.write(g + " : " + password + "\n")
    f.close()

    print(g)
    return g


if __name__=="__main__":
    main()
